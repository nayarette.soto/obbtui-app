export const GlobalVar = Object.freeze({
  BASE_API_URL: 'http://localhost:8080/apptui/',
  PROD_MODE: true,
  GANALYTICS_API_KEY: 'ganalytics_apikey',
  PORTAL_ACADEMICO: 'http://portal.uv.cl/loginPregrado',
  OLVIDAR_CONTRASENA: 'http://cambiaclave.uv.cl',
  FIREBASE_PID: 'firebase-id',
  ONESIGNAL_ID: 'onesignal-id'
});
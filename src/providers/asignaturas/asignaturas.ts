import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVar } from '../../config';

@Injectable()
export class AsignaturasProvider {
  url: string;

  constructor(public http: HttpClient) {
    let env = this;
    env.url = GlobalVar.BASE_API_URL + 'asignaturas/';
  }

  getAsignaturas(token: string): Observable<any> {
    let env = this;
    let headers = { headers: new HttpHeaders().set('Authorization', token) };
    return env.http
      .post(env.url, null, headers)
      .map(function(response) { return response })
      .catch(env.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }
}
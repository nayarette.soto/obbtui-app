import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import * as sha1 from 'js-sha1';


@Component({
  selector: 'credencial',
  templateUrl: 'credencial.html'
})
export class CredencialComponent {
  @ViewChild('anverso') anverso;
  @ViewChild('informacion') info;
  width: number;
  height: number;
  credencial: object;
  QRData: string;
  random: number;

  constructor(platform: Platform) {
    let env = this;
    let fecha = new Date();
    let str_fecha = fecha.getDate() + '-' + fecha.getMonth() + '-' + fecha.getFullYear() + 'T' +
      fecha.getHours() + ':' + fecha.getMinutes();

    env.credencial = {
      foto: localStorage.getItem('fotografia'),
      nombre: localStorage.getItem('nombres').split(' ')[0] +
        ' ' + localStorage.getItem('apellidos'),
      carrera: localStorage.getItem('carrera'),
      run: env.formatearRUN(localStorage.getItem('run'))
    };

    // Generar valor aleteatorio
    env.random = Math.floor(Math.random() * (9999999999999 - 1000000000000 + 1)) + 1000000000000;

    // Generar data de código QR
    env.QRData = 'UV' + fecha.getUTCFullYear() + env.random + ',' + localStorage.getItem('run') + ',' + sha1(str_fecha);
    console.log('QRData: ', env.QRData);
  }

  ngAfterViewInit() {
    let env = this;
    console.log('Anverso-Alto: ', env.anverso.nativeElement.offsetHeight);
    console.log('Anverso-Ancho: ', env.anverso.nativeElement.offsetWidth);


    env.height = env.anverso.nativeElement.offsetWidth;
    env.width = env.anverso.nativeElement.offsetHeight - 44;

    console.log('Info-Alto: ', env.info.nativeElement.offsetHeight);
    console.log('Info-Ancho: ', env.info.nativeElement.offsetWidth);
  }

  calcularDV(numero) {
    var nuevo_numero = numero.toString().split('').reverse().join('');
    for(var i = 0, j = 2, suma = 0; i < nuevo_numero.length; i++, ((j == 7) ? j = 2 : j++)) {
      suma += (parseInt(nuevo_numero.charAt(i)) * j);
    }
    var n_dv = 11 - (suma % 11);
    return ((n_dv == 11) ? 0 : ((n_dv == 10) ? 'K' : n_dv));
  }

  formatearRUN(run) {
    let env = this;
    console.log(run);
    run = run.concat(env.calcularDV(run));
    console.log(run);
    console.log(env.calcularDV(run));
    var actual = run.replace(/^0+/, '');
    if (actual != '' && actual.length > 1) {
      var sinPuntos = actual.replace(/\./g, '');
      var actualLimpio = sinPuntos.replace(/-/g, '');
      var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
      var runPuntos = '';
      var i = 0;
      var j = 1;
      for(i = inicio.length - 1; i >= 0; i--) {
        var letra = inicio.charAt(i);
        runPuntos = letra + runPuntos;
        if(j % 3 == 0 && j <= inicio.length - 1) {
          runPuntos = '.' + runPuntos;
        }
        j++;
      }
      var dv = actualLimpio.substring(actualLimpio.length - 1);
      runPuntos = runPuntos + "-" + dv;
    }
    return runPuntos;
  }
}
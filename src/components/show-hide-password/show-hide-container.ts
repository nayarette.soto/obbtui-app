import { Component, ContentChild } from '@angular/core';
import { ShowHideInput } from './show-hide-input'

@Component({
  selector: 'show-hide-container',
  templateUrl: 'show-hide-password.html',
  host: {
    'class': 'show-hide-password'
  }
})
export class ShowHideContainer {
  show = false;

  @ContentChild(ShowHideInput) input: ShowHideInput;

  constructor(){}

  toggleShow() {
    let env = this;
    env.show = !env.show;
    if (env.show){
      env.input.changeType('text');
    } else {
      env.input.changeType('password');
    }
  }
}

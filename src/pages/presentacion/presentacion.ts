import { Component, ViewChild } from '@angular/core';
import { LoadingController, NavController, Slides } from 'ionic-angular';
import { AccesoPage } from '../acceso/acceso';

@Component({
  selector: 'presentacion-page',
  templateUrl: 'presentacion.html'
})
export class PresentacionPage {
  loading: any;
  lastSlide = false;
  @ViewChild('slider') slider: Slides;

  constructor(
      public nav: NavController,
      public loadingCtrl: LoadingController,
  ) {
    let env = this;
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
  }

  skipIntro() {
    let env = this;
    // You can skip to main app
    // this.nav.setRoot(TabsNavigationPage);

    // Or you can skip to last slide (login/signup slide)
    env.lastSlide = true;
    env.slider.slideTo(env.slider.length());
  }

  onSlideChanged() {
    let env = this;
    // If it's the last slide, then hide the 'Skip' button on the header
    env.lastSlide = env.slider.isEnd();
  }

  acceder() {
    let env = this;
    localStorage.setItem('presentacion', 'false');
    env.nav.setRoot(AccesoPage);
    env.nav.popToRoot();
  }
}